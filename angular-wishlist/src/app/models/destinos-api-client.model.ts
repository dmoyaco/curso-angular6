import { DestinoViaje } from "src/app/models/destino-viaje.model";

export class DestinosApiClient {

    private destinos: DestinoViaje[];

    constructor() {
        this.destinos = [];
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    add(dest: DestinoViaje) {
        this.destinos.push(dest);
    }

}