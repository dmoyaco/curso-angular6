export class DestinoViaje {
    private selected: boolean;
    private servicios: string[];

    constructor(public nombre: string, public imagenURL: string) {
        this.servicios = ['Piscina', 'Desayuno'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(sel: boolean): void {
        this.selected = sel;
    }

    getServicios(): string[] {
        return this.servicios;
    }

    addServicio(serv: string): void {
        this.servicios.push(serv);
    }

    clearServicios(): void {
        this.servicios = [];
    }
}