import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  ciudades: string[];

  constructor(public destinosApiClient: DestinosApiClient) { 
    this.onItemAdded = new EventEmitter();
    this.ciudades = ['Madrid','Barcelona','Granada','Valencia'];
  }

  ngOnInit(): void {
  }

  agregar(dest: DestinoViaje) {
    this.destinosApiClient.add(dest);
    this.onItemAdded.emit(dest);
  }

  elegido(dest: DestinoViaje) {
    this.destinosApiClient.getAll().forEach(function(element) {
      element.setSelected(false);
    });
    dest.setSelected(true);
  }

}
